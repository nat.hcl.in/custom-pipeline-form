### Main Form

<div id="pipeline-form">
  <form action="#" method="post">
    <p>Choose your favorite dessert:</p>
    <select id="dessert">
      <option>Apple Pie</option>
      <option>Key Lime Pie</option>
      <option>Éclair</option>
      <option>Cheesecake</option>
      <option>Banana Pudding</option>
    </select>
    <button type="button" id="api-example">Run the Pipeline</button>
  </form>
</div>
<div id="pipeline-after" style="display:none;">
  <p id="api-output">Please stand by...</p>
</div>
  
